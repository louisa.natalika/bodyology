from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from homepage.models import CustomUser
from reviewPage.models import *
from reviewPage.urls import *
from reviewPage.forms import *
from reviewPage.views import *

from .apps import ReviewpageConfig

# Create your tests here.
class ReviewTest(TestCase):
    def create_model(self):
        return ReviewModel.objects.create(
            produknya = "test01",
            namaReview = "testnama01",
            isiReview = "testisi01",
        )

    def test_reviewPage_app(self):
        self.assertEqual(ReviewpageConfig.name, 'reviewPage')
        self.assertEqual(apps.get_app_config('reviewPage').name, 'reviewPage')

    def test_str_model(self):
        review = self.create_model()
        self.assertEqual(str(review), 'test01')

    def test_form_review(self):
        form_data = {'isiReview' : 'testtttttttt'}
        form = ReviewForm(data = form_data)
        self.assertTrue(form.is_valid())

    def test_apakah_ada_url_yang_menampilkan_review(self):
        review = self.create_model()
        c = Client()
        response = c.get('/products/hair/desc/test01/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_user_authenticated_bisa_masuk(self):
        user = CustomUser.objects.create(username='test02')
        user.set_password('teet021410')
        user.save()

        c = Client()
        c.login(username='test02', password='teet021410')
        review = self.create_model()
        response = c.get('/products/hair/desc/test01/')
        self.assertEqual(response.status_code, 200)

        