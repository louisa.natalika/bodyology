from django.db import models

# Create your models here.

class ReviewModel(models.Model):
    namaReview = models.CharField(max_length = 50)
    produknya = models.CharField(max_length = 50)
    isiReview = models.TextField(max_length = 400)

    def __str__(self):
        return self.produknya