# BODYOLOGY

[![pipeline status](https://gitlab.com/louisa.natalika/bodyology/badges/master/pipeline.svg)](https://gitlab.com/louisa.natalika/bodyology/commits/master)

[![coverage report](https://gitlab.com/louisa.natalika/bodyology/badges/master/coverage.svg)](https://gitlab.com/louisa.natalika/bodyology/commits/master)

## Anggota Kelompok:
* Brian Athallah Yudiva (1806205804)  
* Louisa Natalika Jovanna (1806205022)  
* Fitri Handayani (1806146966)  
* Doan Andreas Nathanael (1806205123)  

## Link Heroku
Link : https://bodyology2.herokuapp.com/  

## Deskripsi
 Aplikasi yang kelompok kami buat adalah aplikasi yang mampu memberikan 
 rekomendasi terhadap beberapa permasalahan yang dialami seseorang khususnya 
 kulit wajah, kulit, serta rambut. Dengan menggunakan aplikasi ini, user dapat 
 menerima saran sesuai keadaan mereka tanpa dipungut biaya. Di aplikasi kami, 
 user mendapat informasi mengenai produk serta deskripsinya. User juga dapat 
 diarahkan ke web agar dapat langsung membeli produk yang kami rekomendasikan. 
 Oleh karena itu, aplikasi kami bertujuan untuk membantu user mendapat solusi 
 serta informasi mengenai solusi tersebut secara efisien.  

 Untuk autentikasi (login), gunakan:
 *  Username: admin
 *  Password: admin
 
 ## Fitur:
 *   Form yang bisa dimasukkan sesuai keadaan user
 *   Link menuju website pembelian produk
 *   Informasi mengenai produk
 *   Terdapat summary berdasarkan form yang telah diisi user