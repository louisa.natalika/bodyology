from django.test import TestCase,Client
from django.urls import resolve
from .views import home, logout_view, login_view
from .models import CustomUser

from advice.models import AdviceModel

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

# Create your tests here.
class UnitTestStatusPage(TestCase):
    def test_apakah_ada_url_ada(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_url_home_menggunakan_fungsi_home(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_apakah_url_home_mengembalikan_html_home(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')
        self.assertIn("Welcome", content)
        self.assertNotIn("Thank", content)
    
    def test_apakah_url_logout_menggunakan_fungsi_logout(self):
        found = resolve(reverse('homepage:logout'))
        self.assertEqual(found.func, logout_view)
    
    def test_apakah_CustomUser_Model_berhasil_dibuat(self):
        user =  CustomUser.objects.create(username='admin', password='admin')
        seluruh_user = CustomUser.objects.all()
        self.assertEqual(seluruh_user.count(), 1)
        self.assertEqual(CustomUser.objects.get(id=1).username, 'admin')
    
    def test_apakah_url_login_menggunakan_fungsi_login(self):
        found = resolve(reverse('homepage:login'))
        self.assertEqual(found.func, login_view)
    
        
    
class FunctionalTestAccounts(StaticLiveServerTestCase):
    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)
        user = CustomUser.objects.create(username='admin', password='admin')
        advice = AdviceModel.objects.create(namaUser='admin', categoryAdvice='Review Website', isiAdvice="Websitenya keren")

    def tearDown(self):
        self.browser.close()
    
    def test_jquery_info_tab(self):
        self.browser.get(self.live_server_url)
        info_tab = self.browser.find_element_by_id('info-tab')
        info_tab.click()
        self.assertIn("More", self.browser.page_source)

    def test_jquery_login_tab(self):
        self.browser.get(self.live_server_url)
        info_tab = self.browser.find_element_by_id('login-tab')
        info_tab.click()
        self.assertIn("username", self.browser.page_source)
    
    def test_review_muncul(self):
        self.browser.get(self.live_server_url)
        self.assertIn("Websitenya", self.browser.page_source)

    def test_ada_title_website(self):
        # User membuka browser dengan mengakses url yang mereka ketahui
        self.browser.get(self.live_server_url)

        # User melihat title website yang dibuka tertulis Meet Lika
        self.assertIn('BODYOLOGY', self.browser.title)
    
    def test_bisa_login(self):
        # User membuka browser dengan mengakses url login
        self.browser.get(self.live_server_url)

        userField = self.browser.find_element_by_name('username')
        passField = self.browser.find_element_by_name('password')

        #Memasukkan input ke form
        userField.send_keys('admin')
        passField.send_keys('admin')
        
        button = self.browser.find_element_by_id('login-button')
        button.click()