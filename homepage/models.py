# users/models.py
from django.contrib.auth.models import AbstractUser
from django.db import models
from products.models import hairCategory,faceCategory,skinCategory

class CustomUser(AbstractUser):
    hair = models.ManyToManyField(hairCategory)
    face = models.ManyToManyField(faceCategory)
    skin = models.ManyToManyField(skinCategory)