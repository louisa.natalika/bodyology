from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import CustomUserAuthenticationForm

# Create your views here.
def home(request):
    form = CustomUserAuthenticationForm()
    arguments = {
        'form' : form,
    }
    return render(request, 'home.html', arguments)

def login_view(request):
    if request.method == "POST":
        form = CustomUserAuthenticationForm(data=request.POST)
        if form.is_valid():
            #log in the user
            user = form.get_user()
            login(request, user)
            return redirect('homepage:home')
    form = CustomUserAuthenticationForm()
    arguments = {
        'form' : form,
    }
    return render(request, 'home.html', arguments)

def logout_view(request):
    logout(request)
    return redirect('homepage:home')