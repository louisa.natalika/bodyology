$(document).ready(function(){

    $.ajax({
        method:'GET',
        url:'advice/getData/',
        success: function(response){
            for(let i = 0; i < response.length; i++){
                console.log("data ke " + i );
                if(response[i].fields.categoryAdvice == "Review Website"){
                    let name = document.createElement('h2')
                    let suggestion = document.createElement('p')
                    let line = document.createElement('hr')
                    name.innerText = response[i].fields.namaUser
                    suggestion.innerText = response[i].fields.isiAdvice
                    $('.card-container').append(name)
                    $('.card-container').append(suggestion)
                    $('.card-container').append(line)

                }                
            }
        }
    });
    
    
    

});

$("#info-content").hide();
$("#login-tab").addClass("active");

$("#login-tab").click(function(){
    $("#info-tab").removeClass("active");
    $("#login-tab").addClass("active");
    $("#info-content").hide();
    $("#login-content").show();
})

$("#info-tab").click(function(){
    $("#login-tab").removeClass("active");
    $("#info-tab").addClass("active");
    $("#login-content").hide();
    $("#info-content").show();
})


$("#login-tab").hover(function(){
    $(this).css("font-style", "italic");
    }, function(){
    $(this).css("font-style", "normal");
});

$("#info-tab").hover(function(){
    $(this).css("font-style", "italic");
    }, function(){
    $(this).css("font-style", "normal");
});

$("input").focus(function(){
    $("#join").css("display", "inline").fadeOut(2000);
});