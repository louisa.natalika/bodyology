from django.test import TestCase, Client
from django.urls import resolve, reverse

from homepage.models import CustomUser
from .views import *
from .models import hairCategory, faceCategory, skinCategory, product
from .forms import hairCategoryForm, faceCategoryForm, skinCategoryForm

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.auth.models import User

# Create your tests here.
class HairCategoryPage(TestCase):
    def create_model(self):
        return product.objects.create(
            nama_merk = 'testHair',
            nama_produk = 'subjudulHair',
            deskripsi = 'deskripsiHair',

            kategori = 'HAIR',
            kategoriHair = 'DANDRUFF',
            kategoriFace = 'null',
            kategoriSkin = 'null',

            linkBeli = '#',
            linkImage = '#',
        )
        
    def test_apakah_ada_url_yang_menampilkan_products_hair(self):
        c = Client()
        response = c.get('/products/hair/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('products/hair.html')

    def test_apakah_ada_tulisan_hello(self):
        user = CustomUser.objects.create(username='admin')
        user.set_password('admin')
        user.save()

        c = Client()
        c.login(username='admin', password='admin')

        response = c.get('/products/hair/')
        content = response.content.decode('utf-8')
        self.assertIn('Hello', content)

    def test_apakah_terdapat_selectbox_untuk_memilih_kategori_rambut(self):
        user = CustomUser.objects.create(username='admin')
        user.set_password('admin')
        user.save()

        c = Client()
        c.login(username='admin', password='admin')

        response = c.get('/products/hair/')
        content = response.content.decode('utf-8')
        self.assertIn("hairCategory", content)

    def test_apakah_terdapat_button_submit(self):
        user = CustomUser.objects.create(username='admin')
        user.set_password('admin')
        user.save()

        c = Client()
        c.login(username='admin', password='admin')

        response = c.get('/products/hair/')
        content = response.content.decode('utf-8')
        self.assertIn("<button", content)
        self.assertIn("submit", content)

    def test_apakah_models_produk_hair_berjalan(self):
        hairProduct = self.create_model()
        count = product.objects.all().count()
        self.assertEqual(count, 1)


class FaceCategoryPage(TestCase):
    def test_apakah_ada_url_yang_menampilkan_products_face(self):
        c = Client()
        response = c.get('/products/face/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('products/face.html')

    def test_apakah_ada_tulisan_hello(self):
        user = CustomUser.objects.create(username='admin')
        user.set_password('admin')
        user.save()

        c = Client()
        c.login(username='admin', password='admin')

        response = c.get('/products/face/')
        content = response.content.decode('utf-8')
        self.assertIn('Hello', content)

    def test_apakah_terdapat_selectbox_untuk_memilih_kategori_faceskin(self):
        user = CustomUser.objects.create(username='admin')
        user.set_password('admin')
        user.save()

        c = Client()
        c.login(username='admin', password='admin')

        response = c.get('/products/face/')
        content = response.content.decode('utf-8')
        self.assertIn("faceCategory", content)

    def test_apakah_terdapat_button_submit(self):
        user = CustomUser.objects.create(username='admin')
        user.set_password('admin')
        user.save()

        c = Client()
        c.login(username='admin', password='admin')
        
        response = c.get('/products/face/')
        content = response.content.decode('utf-8')
        self.assertIn("<button", content)
        self.assertIn("submit", content)

class SkinCategoryPage(TestCase):
    def test_apakah_ada_url_yang_menampilkan_products_skin(self):
        c = Client()
        response = c.get('/products/skin/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('products/skin.html')

    def test_apakah_ada_tulisan_hello(self):
        user = CustomUser.objects.create(username='admin')
        user.set_password('admin')
        user.save()

        c = Client()
        c.login(username='admin', password='admin')

        response = c.get('/products/skin/')
        content = response.content.decode('utf-8')
        self.assertIn('Hello', content)

    def test_apakah_terdapat_selectbox_untuk_memilih_kategori_skin(self):
        user = CustomUser.objects.create(username='admin')
        user.set_password('admin')
        user.save()

        c = Client()
        c.login(username='admin', password='admin')

        response = c.get('/products/skin/')
        content = response.content.decode('utf-8')
        self.assertIn("skinCategory", content)

    def test_apakah_terdapat_button_submit(self):
        user = CustomUser.objects.create(username='admin')
        user.set_password('admin')
        user.save()

        c = Client()
        c.login(username='admin', password='admin')

        response = c.get('/products/skin/')
        content = response.content.decode('utf-8')
        self.assertIn("<button", content)
        self.assertIn("submit", content)
