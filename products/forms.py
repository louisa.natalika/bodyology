from django import forms
from .models import hairCategory, faceCategory, skinCategory

class hairCategoryForm(forms.ModelForm):
    class Meta:
        model = hairCategory
        fields = {'hairCategory'}
        widgets = {'hairCategory': forms.Select(
                    attrs = {
                        'class': 'form-control',
                    }
                )}

class faceCategoryForm(forms.ModelForm):
    class Meta:
        model = faceCategory
        fields = {'faceCategory'}
        widgets = {'faceCategory': forms.Select(
                    attrs = {
                        'class': 'form-control',
                    }
                )}

class skinCategoryForm(forms.ModelForm):
    class Meta:
        model = skinCategory
        fields = {'skinCategory'}
        widgets = {'skinCategory': forms.Select(
                    attrs = {
                        'class': 'form-control',
                    }
                )}