from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.core import serializers
# from homepage.models import Person
from .models import hairCategory, faceCategory, skinCategory, product
from .forms import hairCategoryForm, faceCategoryForm, skinCategoryForm

def getDataHair(request):
    data_produk = product.objects.filter(kategori='HAIR')
    produk_list = serializers.serialize('json', data_produk)
    return HttpResponse(produk_list, content_type="text/json-comment-filtered")

def getDataFace(request):
    data_produk = product.objects.filter(kategori='FACE')
    produk_list = serializers.serialize('json', data_produk)
    return HttpResponse(produk_list, content_type="text/json-comment-filtered")

def getDataSkin(request):
    data_produk = product.objects.filter(kategori='SKIN')
    produk_list = serializers.serialize('json', data_produk)
    return HttpResponse(produk_list, content_type="text/json-comment-filtered")

def hair(request):
    query = request.GET.get("hairCategory")

    if query:
        hair = product.objects.filter(kategoriHair=query)
        argument = {
            'postResult' : True,
            'query' : query,
        }

        return render(request, 'hair.html', argument)
    
    argument = {
        'hairCategory' : hairCategoryForm(),
        'postResult' : False,
    }

    return render(request, 'hair.html', argument)

def face(request):
    query = request.GET.get("faceCategory")

    if query:
        face = product.objects.filter(kategoriFace=query)
        argument = {
            'postResult' : True,
            'query' : query,
        }

        return render(request, 'face.html', argument)
    
    argument = {
        'faceCategory' : faceCategoryForm(),
        'postResult' : False,
    }

    return render(request, 'face.html', argument)

def skin(request):
    query = request.GET.get("skinCategory")

    if query:
        skin = product.objects.filter(kategoriSkin=query)
        argument = {
            'postResult' : True,
            'query' : query,
        }

        return render(request, 'skin.html', argument)
    
    argument = {
        'skinCategory' : skinCategoryForm(),
        'postResult' : False,
    }

    return render(request, 'skin.html', argument)

