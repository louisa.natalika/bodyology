from django.db import models

category = {
    ('HAIR', 'HAIR'),
    ('FACE', 'FACE'),
    ('SKIN', 'SKIN')
}

hairDict = {
    ('null', 'null'),
    ('DANDRUFF', 'DANDRUFF'),
    ('DRY HAIR', 'DRY HAIR'),
    ('HAIR LOSS', 'HAIR LOSS')
}

faceDict = {
    ('null', 'null'),
    ('JERAWAT', 'JERAWAT'),
    ('BERMINYAK', 'BERMINYAK'),
    ('KERING', 'KERING'),
    ('NORMAL', 'NORMAL'),
}

skinDict = {
    ('null', 'null'),
    ('NORMAL', 'NORMAL'),
    ('KERING', 'KERING'),
    ('SENSITIF', 'SENSITIF'),
}

# Create your models here.
class hairCategory(models.Model):
    # nama = models.CharField(max_length=20)
    hairCategory = models.CharField(max_length=20, choices=hairDict)

    def __str__(self):
        return self.hairCategory

class faceCategory(models.Model):
    # nama = models.CharField(max_length=20)
    faceCategory = models.CharField(max_length=20, choices=faceDict)

    def __str__(self):
        return self.faceCategory

class skinCategory(models.Model):
    # nama = models.CharField(max_length=20)
    skinCategory = models.CharField(max_length=20, choices=skinDict)

    def __str__(self):
        return self.skinCategory

class product(models.Model):
    nama_merk = models.CharField(max_length=100)
    nama_produk = models.CharField(max_length=200)
    deskripsi = models.TextField(max_length=1000)

    kategori = models.CharField(max_length=50, choices=category)
    kategoriHair = models.CharField(max_length=50, choices=hairDict)
    kategoriFace = models.CharField(max_length=50, choices=faceDict)
    kategoriSkin = models.CharField(max_length=50, choices=skinDict)

    linkBeli = models.CharField(max_length=1000)
    linkImage = models.CharField(max_length=1000)
    linkReview = models.CharField(max_length=1000)

    def __str__(self):
        return self.nama_merk+" --> "+self.nama_produk