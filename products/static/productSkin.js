$(document).ready(function(){
    console.log("masuk SKIN khintil");
    $('#product').empty()
    let pesan = document.createElement('h3')
    pesan.innerText = "Loading..."
    $('#product').append(pesan)

    let query = $('#query').text()
    console.log(query)

    $.ajax({
        method:'GET',
        url:'getData/',
        success: function(response){
            $('#product').empty()
            for(let i = 0; i < response.length; i++){
                if(response[i].fields.kategoriSkin == query){
                    let cardDiv = document.createElement('div');
                    cardDiv.className = 'card';
                    cardDiv.style = 'width: 35vw'

                    let img = document.createElement('img');
                    img.src = response[i].fields.linkImage;
                    img.className = 'card-img-top cardImage';
                    img.style = 'width: 200px; height: auto;'

                    let divContent = document.createElement('div');
                    divContent.className = 'card-body'
                    
                    let nama = document.createElement('h5')
                    nama.innerText = response[i].fields.nama_merk;
                    nama.className = 'card-title'

                    let subnama = document.createElement('h6')
                    subnama.innerText = response[i].fields.nama_produk;
                    subnama.className = "card-subtitle mb-2 text-muted"

                    let desc = document.createElement('p')
                    desc.innerText = response[i].fields.deskripsi
                    desc.className = "card-text"

                    let reviewButton = document.createElement('a')
                    reviewButton.innerText = 'Review'
                    reviewButton.className = "btn btn-primary"
                    reviewButton.href = "desc/" + response[i].fields.nama_merk + "/"

                    let beliButton = document.createElement('a')
                    beliButton.innerText = 'Beli produk ini'
                    beliButton.className = "btn btn-primary"
                    beliButton.href = response[i].fields.linkBeli

                    divContent.appendChild(nama)
                    divContent.appendChild(subnama)
                    divContent.appendChild(desc)
                    divContent.appendChild(reviewButton)
                    divContent.appendChild(beliButton)

                    cardDiv.appendChild(img);
                    cardDiv.appendChild(nama);
                    cardDiv.appendChild(subnama);
                    cardDiv.appendChild(divContent);

                    $('#product').append(cardDiv);

                    $(".btn").hover(function() { 
                        console.log('masuk hover button')
                        $(this).removeClass('btn-primary'); 
                        $(this).addClass('btn-success');
                    }, function() { 
                        $(this).addClass('btn-primary'); 
                        $(this).removeClass('btn-success');
                    }); 
                }
            }
        }
    });

    $(".btn").hover(function() { 
        console.log('masuk hover button')
        $(this).removeClass('btn-primary'); 
        $(this).addClass('btn-success');
    }, function() { 
        $(this).addClass('btn-primary'); 
        $(this).removeClass('btn-success');
    }); 
});