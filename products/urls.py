from django.urls import path
from . import views

app_name = 'products'

urlpatterns = [
    path('hair/', views.hair, name='hair'),
    path('face/', views.face, name='face'),
    path('skin/', views.skin, name='skin'),
    path('hair/getData/', views.getDataHair, name='getDataHair'),
    path('face/getData/', views.getDataFace, name='getDataFace'),
    path('skin/getData/', views.getDataSkin, name='getDataSkin'),
]