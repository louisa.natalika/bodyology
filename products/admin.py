from django.contrib import admin
from products.models import *

# Register your models here.
admin.site.register(hairCategory)
admin.site.register(faceCategory)
admin.site.register(skinCategory)
admin.site.register(product)
