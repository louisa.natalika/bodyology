from django import forms
from .models import AdviceModel

class AdviceForm(forms.ModelForm):
    class Meta:
        model = AdviceModel
        fields = {'namaUser','categoryAdvice','isiAdvice'}
        widgets = {'namaUser': forms.TextInput(
                    attrs = {
                        'placeholder':'Type your name or initial here',
                        'class': 'form-control',
                    }
                ),'categoryAdvice': forms.Select(
                    attrs = {
                        'class': 'form-control',
                    }
                ),
                    'isiAdvice': forms.Textarea(
                        attrs = {
                            'placeholder':'Give your advice here',
                            'class': 'form-control',
                        }
                    )}
