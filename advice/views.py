from django.shortcuts import render, redirect, HttpResponse
from django.core import serializers
from .forms import AdviceForm
from .models import AdviceModel

# Create your views here.
def index_advice(request):
    advice_form = AdviceForm()
    argument = {
        'advice_form' : advice_form
    }
    return render(request, 'advice/index.html', argument)

def terima_advice(request):
    if request.method == 'POST':
        advice_form = AdviceForm(request.POST)
        if advice_form.is_valid():
            advice_form.save()
    else:
        advice_form = AdviceForm()

    data = AdviceModel.objects.all()
    argument = {
        'data': data,
        'advice_form' : advice_form
    }

    return render(request, 'advice/index.html', argument)
    

def getData(request):
    dataAdvice = AdviceModel.objects.all()
    advice_list = serializers.serialize('json', dataAdvice)
    return HttpResponse(advice_list, content_type="text/json-comment-filtered")

