from django.db import models

adviceCategory = {
    ('Suggestion', 'Suggestion'),
    ('Review Website', 'Review Website')
}
# Create your models here.
class AdviceModel(models.Model):
    namaUser = models.CharField(max_length=20, default='')
    categoryAdvice = models.CharField(max_length=20, choices=adviceCategory, default='')
    isiAdvice = models.CharField(max_length=2000, default='')

    def __str__(self):
        return self.categoryAdvice